package com.conta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conta.entity.Serviciu;

@Repository
public interface ServiciuRepository extends JpaRepository<Serviciu, Long> {

}
