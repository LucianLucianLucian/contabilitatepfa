package com.conta.repository;

import com.conta.entity.Sediu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SediuRepository extends JpaRepository<Sediu, Long> {
}