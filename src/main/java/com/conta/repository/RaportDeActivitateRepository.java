package com.conta.repository;

import com.conta.entity.RaportDeActivitate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaportDeActivitateRepository extends JpaRepository<RaportDeActivitate, Long> {
}
