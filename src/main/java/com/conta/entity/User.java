package com.conta.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3129576259521536701L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Column
	private String name;

	@Column
	private String cif;

	@Column
	private String cui;

	@ManyToOne
	@JsonIgnore
	private Sediu sediu;

	@Column
	private Judet judet;

	@OneToMany(mappedBy = "user")
	private Set<Factura> facturi;

	@Column(unique = true, nullable = false)
	private String email;

	@Column(nullable = false)
	private String password;

	@Column(unique = true, nullable = false)
	private String userName;

	@Column(nullable = false)
	private int active;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private List<Role> roles = new ArrayList<Role>();

	public User() {
	}

	public User(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.cif = user.getCif();
		this.cui = user.getCui();
		this.sediu = user.getSediu();
		this.judet = user.getJudet();
		this.facturi = user.getFacturi();
		this.email = user.getEmail();
		this.password = user.getPassword();
		this.userName = user.getUsername();
		this.active = user.getActive();
		this.roles = user.getRoles();
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		return getRoles().stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName()))
				.collect(Collectors.toList());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	public long getId() {
		return id;
	}

	public String getCif() {
		return cif;
	}

	public String getCui() {
		return cui;
	}

	public Sediu getSediu() {
		return sediu;
	}

	public Judet getJudet() {
		return judet;
	}

	public Set<Factura> getFacturi() {
		return facturi;
	}

	public String getEmail() {
		return email;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	public void setSediu(Sediu sediu) {
		this.sediu = sediu;
	}

	public void setJudet(Judet judet) {
		this.judet = judet;
	}

	public void setFacturi(Factura factura) {
		this.facturi.add(factura);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
