package com.conta.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Cumparator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String nume;

    @Column
    private String cif;

    @Column
    private String cui;

    @Column
    private String adresa;

    @Column
    private Judet judetul;

    @OneToMany(mappedBy = "cumparator", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Factura> factura = new HashSet<>();

    public long getId() {
        return id;
    }

    public String getNume() {
        return nume;
    }

    public String getCif() {
        return cif;
    }

    public String getCui() {
        return cui;
    }

    public Judet getJudetul() {
        return judetul;
    }

    public Set<Factura> getFactura() {
        return factura;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public void setJudetul(Judet judetul) {
        this.judetul = judetul;
    }

    public void setFactura(Factura factura) {
        this.factura.add(factura);
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }
}
