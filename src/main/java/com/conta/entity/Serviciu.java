package com.conta.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Serviciu {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Column
	private String activitate;

	@Column
	private int usi;

	@Column
	private float tarifEuroPerUsi;

	@Column
	private float totalEuro;

	@ManyToOne
	@JsonIgnore
	private RaportDeActivitate raportDeActivitate;

	public RaportDeActivitate getRaportDeActivitate() {
		return raportDeActivitate;
	}

	public void setRaportDeActivitate(RaportDeActivitate raportDeActivitate) {
		this.raportDeActivitate = raportDeActivitate;
	}

	public String getActivitate() {
		return activitate;
	}

	public void setActivitate(String activitate) {
		this.activitate = activitate;
	}

	public int getUsi() {
		return usi;
	}

	public void setUsi(int usi) {
		this.usi = usi;
	}

	public float getTarifEuroPerUsi() {
		return tarifEuroPerUsi;
	}

	public void setTarifEuroPerUsi(float tarifEuroPerUsi) {
		this.tarifEuroPerUsi = tarifEuroPerUsi;
	}

	public float getTotalEuro() {
		return totalEuro;
	}

	public float setTotalEuro() {
		totalEuro = tarifEuroPerUsi * usi;
		return totalEuro;
	}

	public long getId() {
		return id;
	}

}
