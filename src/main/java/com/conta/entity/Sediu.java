package com.conta.entity;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Sediu")
public class Sediu {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @Column
    private String strada;

    @Column
    private int nr;

    @Column
    private String bloc;

    @Column
    private String scara;

    @Column
    private String apartament;

    @Column
    private String oras;

    @OneToMany(mappedBy = "sediu", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<User> users;

    public long getId() {
        return id;
    }

    public String getStrada() {
        return strada;
    }

    public int getNr() {
        return nr;
    }

    public String getBloc() {
        return bloc;
    }

    public String getScara() {
        return scara;
    }

    public String getApartament() {
        return apartament;
    }

    public String getOras() {
        return oras;
    }

    public void setStrada(String strada) {
        this.strada = strada;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public void setBloc(String bloc) {
        this.bloc = bloc;
    }

    public void setScara(String scara) {
        this.scara = scara;
    }

    public void setApartament(String apartament) {
        this.apartament = apartament;
    }

    public void setOras(String oras) {
        this.oras = oras;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(User user) {
        this.users.add(user);
    }

}
