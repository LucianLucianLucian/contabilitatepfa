package com.conta.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import org.omg.CORBA.CTX_RESTRICT_SCOPE;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Factura {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String serie;

    @Column
    @Temporal(TemporalType.DATE)
    @CreatedDate
    private Date dataCrearii;

    @Column
    private float total;

    @OneToOne
    private RaportDeActivitate raportDeActivitate;

    @OneToMany(mappedBy = "factura")
    private Set<Produs> produse;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @JsonIgnore
    private Cumparator cumparator;

    @ManyToOne
    @JsonIgnore
    private User user;


    public long getId() {
        return id;
    }

    public String getSerie() {
        return serie;
    }

    public Date getDataCrearii() {
        return dataCrearii;
    }

    public float getTotal() {
        return total;
    }

    public RaportDeActivitate getRaportDeActivitate() {
        return raportDeActivitate;
    }

    public Set<Produs> getProduse() {
        return produse;
    }

    public Cumparator getCumparator() {
        return cumparator;
    }

    public User getUser() {
        return user;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public void setDataCrearii(Date dataCrearii) {
        this.dataCrearii = dataCrearii;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public void setRaportDeActivitate(RaportDeActivitate raportDeActivitate) {
        this.raportDeActivitate = raportDeActivitate;
    }

    public void setProduse(Set<Produs> produse) {
        this.produse = produse;
    }

    public void setCumparator(Cumparator cumparator) {
        this.cumparator = cumparator;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
