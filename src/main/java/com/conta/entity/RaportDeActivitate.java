package com.conta.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

@Entity
public class RaportDeActivitate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Column
	private String text1;

	@Column
	@Temporal(TemporalType.DATE)
	private Date inceputRaportDeActivitate;

	@Column
	@Temporal(TemporalType.DATE)
	private Date sfarsitRaportDeActivitate;

	@Column
	@Temporal(TemporalType.DATE)
	@CreatedDate
	private Date dataCrearii;

	@Column
	private String text2;

	@Column
	private String concluzii;

	@OneToMany(mappedBy = "raportDeActivitate")
	private Set<Serviciu> servicii;

	@OneToOne(mappedBy = "raportDeActivitate")
	private Factura factura;

	@Column
	private float cursValutar;

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public Date getInceputRaportDeActivitate() {
		return inceputRaportDeActivitate;
	}

	public void setInceputRaportDeActivitate(Date inceputRaportDeActivitate) {
		this.inceputRaportDeActivitate = inceputRaportDeActivitate;
	}

	public Date getSfarsitRaportDeActivitate() {
		return sfarsitRaportDeActivitate;
	}

	public void setSfarsitRaportDeActivitate(Date sfarsitRaportDeActivitate) {
		this.sfarsitRaportDeActivitate = sfarsitRaportDeActivitate;
	}

	public Date getDataCrearii() {
		return dataCrearii;
	}

	public void setDataCrearii(Date dataCrearii) {
		this.dataCrearii = dataCrearii;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}

	public String getConcluzii() {
		return concluzii;
	}

	public void setConcluzii(String concluzii) {
		this.concluzii = concluzii;
	}

	public Set<Serviciu> getServicii() {
		return servicii;
	}

	public void setServicii(Serviciu serviciu) {
		this.servicii.add(serviciu);
	}

	public float getCursValutar() {
		return cursValutar;
	}

	public void setCursValutar(float cursValutar) {
		this.cursValutar = cursValutar;
	}

	public long getId() {
		return id;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

}
