package com.conta;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.conta.repository.SediuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContabilitatePfaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContabilitatePfaApplication.class, args);
    }

}
