package com.conta.controller;

import com.conta.entity.Factura;
import com.conta.repository.FacturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@PreAuthorize("hasAnyRole('USER')")
@CrossOrigin
@RestController
@RequestMapping("/factura")
public class FacturaController {

    private FacturaRepository facturaRepository;

    @Autowired
    public FacturaController(FacturaRepository facturaRepository) {
        this.facturaRepository = facturaRepository;
    }

    @GetMapping("/all")
    public List<Factura> getAll() {
        return facturaRepository.findAll();
    }

    @PostMapping
    public void save(@RequestBody Factura f) {
        facturaRepository.save(f);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        facturaRepository.deleteById(id);
    }

    @GetMapping("/{id}")
    public Factura getById(@PathVariable long id) {
        return facturaRepository.getOne(id);
    }
}
