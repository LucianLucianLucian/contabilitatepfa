package com.conta.controller;


import com.conta.entity.Produs;
import com.conta.repository.ProdusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@PreAuthorize("hasAnyRole('USER')")
@RestController
@RequestMapping("/produs")
public class ProdusController {

    private ProdusRepository produsRepository;

    @Autowired
    public ProdusController( ProdusRepository produsRepository){
        this.produsRepository = produsRepository;
    }

    @RequestMapping("/all")
    public List<Produs> getAllProduse() {
        return produsRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Produs> getProdusById(@PathVariable long id) {
        return produsRepository.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        produsRepository.deleteById(id);
    }

    @PostMapping
    public Produs save(@RequestBody Produs p) {
        return produsRepository.save(p);
    }

}

