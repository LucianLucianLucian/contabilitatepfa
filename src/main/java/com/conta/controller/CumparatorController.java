package com.conta.controller;

import com.conta.entity.Cumparator;
import com.conta.entity.Factura;
import com.conta.repository.CumparatorRepository;
import com.conta.repository.FacturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@PreAuthorize("hasAnyRole('USER')")
@RestController
@RequestMapping("/cumparator")
@CrossOrigin
public class CumparatorController {

    private CumparatorRepository cumparatorRepository;
    private FacturaRepository facturaRepository;

    @Autowired
    public CumparatorController(CumparatorRepository cumparatorRepository, FacturaRepository facturaRepository) {
        this.cumparatorRepository = cumparatorRepository;
        this.facturaRepository = facturaRepository;
    }

    @GetMapping("/all")
    public List<Cumparator> getAll() {
        return cumparatorRepository.findAll();
    }

    @PostMapping
    public void save(@RequestBody Cumparator c) {
        cumparatorRepository.save(c);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        cumparatorRepository.deleteById(id);
    }

    @GetMapping("/{id}")
    public Cumparator getById(@PathVariable long id) {
        return cumparatorRepository.getOne(id);
    }

    @PatchMapping("/update")
    public Cumparator updateCumparator(@RequestBody Cumparator cumparator) {
        cumparator.getFactura().iterator().next().setCumparator(cumparator);
        return cumparatorRepository.save(cumparator);
    }

}
