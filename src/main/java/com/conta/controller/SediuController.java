package com.conta.controller;

import com.conta.entity.Sediu;
import com.conta.repository.SediuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//@PreAuthorize("hasAnyRole('USER')")
@RestController
@RequestMapping("/sediu")
public class SediuController {


    private SediuRepository sediuRepository;

    @Autowired
    public SediuController(SediuRepository sediuRepository) {
        this.sediuRepository = sediuRepository;
    }

    @RequestMapping("/all")
    public List<Sediu> getAll() {
        return sediuRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Sediu> getById(@PathVariable long id) {
        return sediuRepository.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        sediuRepository.deleteById(id);
    }

    @PostMapping
    public Sediu save(@RequestBody Sediu s) {
        return sediuRepository.save(s);
    }

}
