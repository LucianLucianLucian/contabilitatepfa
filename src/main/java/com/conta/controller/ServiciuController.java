package com.conta.controller;


import com.conta.entity.Serviciu;
import com.conta.repository.ServiciuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@PreAuthorize("hasAnyRole('USER')")
@RestController
@RequestMapping("/Serviciu")
public class ServiciuController {

    private ServiciuRepository serviciuRepository;

    @Autowired
    public ServiciuController(ServiciuRepository serviciuRepository) {
        this.serviciuRepository = serviciuRepository;
    }

    @RequestMapping("/all")
    public List<Serviciu> getAll() {
        return serviciuRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Serviciu> getById(@PathVariable long id) {
        return serviciuRepository.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        serviciuRepository.deleteById(id);
    }

    @PostMapping
    public Serviciu save(@RequestBody Serviciu s) {
        return serviciuRepository.save(s);
    }

}