package com.conta.controller;

import com.conta.entity.RaportDeActivitate;
import com.conta.repository.RaportDeActivitateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@PreAuthorize("hasAnyRole('USER')")
@RestController
@RequestMapping("/raportdeactivitate")
public class RaportDeActivitateController {

    private RaportDeActivitateRepository raportDeActivitateRepository;

    @Autowired
    public RaportDeActivitateController(RaportDeActivitateRepository raportDeActivitateRepository) {
        this.raportDeActivitateRepository = raportDeActivitateRepository;
    }

    @RequestMapping("/all")
    public List<RaportDeActivitate> getAll() {
        return raportDeActivitateRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<RaportDeActivitate> getById(@PathVariable long id) {
        return raportDeActivitateRepository.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        raportDeActivitateRepository.deleteById(id);
    }

    @PostMapping
    public RaportDeActivitate save(@RequestBody RaportDeActivitate r) {
        return raportDeActivitateRepository.save(r);
    }

}
