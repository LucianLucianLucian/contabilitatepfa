package com.conta.controller;

import com.conta.entity.Factura;
import com.conta.entity.Judet;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/judet")
@CrossOrigin
@RestController
public class JudetController {

    @GetMapping("/all")
    public List<Judet> getAll() {
        return Arrays.asList(Judet.values());

    }


}
